#!/usr/bin/env python3

import argparse
import difflib
import os
from packaging import version
import re
import subprocess

gmx_desired_version = '2024.0'

parser = argparse.ArgumentParser(description='Python script to compare output from "gmx hbond" and "gmx hbond-legacy".')
parser.add_argument('-v', '--verbose', type=bool, default=False, help='Verbose mode', action=argparse.BooleanOptionalAction)
args = parser.parse_args()

var_gmx: str

gmxdssp_process: subprocess = subprocess.run('gmx --version', shell=True, capture_output=True, text=True)
version_patterm_gmx: re.compile = re.compile(r'GROMACS version:\s*(.+)\s*')
version_gmx: str = version_patterm_gmx.findall(gmxdssp_process.stdout)[0]
if args.verbose:
    print(f'Detected gmx version {version_gmx}.')
if version.parse(version_gmx) < version.parse(gmx_desired_version):
    raise ImportError(f'Gmx version ({version_gmx}) is lower than required ({gmx_desired_version})!')

input_files_process: subprocess = subprocess.run('ls materials/*.tpr', shell=True, capture_output=True, text=True)
input_files: list['str'] = input_files_process.stdout.replace('materials/','').split('\n')
input_files.remove('')
input_files = [i.split('.tpr')[0] for i in input_files]

gmx_gfs_process: subprocess = subprocess.run('ls results/*', shell=True, capture_output=True, text=True)
gmx_gfs: list['str'] = gmx_gfs_process.stdout.replace('results/','').split('\n')
gmx_gfs.remove('')

if args.verbose:
    print(f'{len(input_files)} file(s) will be analysed: {str(input_files).translate({ord(i): None for i in "[]"})}.')
for i in input_files:
    if args.verbose:
        print(f'Generating output file via gmx hbond for {i}...')
    for j in [('Protein', 'Protein'), ('Protein', 'Water'), ('Water', 'Water')]:
        if args.verbose:
            print(f'{i} {j[0]}-{j[1]}...')
        subprocess.run(f'gmx hbond -f materials/{i}.xtc -s materials/{i}.tpr -o results/{i}.{j[0]}-{j[1]}.ndx -r {j[0]} -t {j[1]} -num results/{i}.{j[0]}-{j[1]}.num.xvg -dist results/{i}.{j[0]}-{j[1]}.dist.xvg -ang results/{i}.{j[0]}-{j[1]}.ang.xvg -dan results/{i}.{j[0]}-{j[1]}.dan.xvg', shell=True, capture_output=True, text=True)
    print(f'Generating output file via gmx hbond-legacy for {i}...')
    for j in [('Protein', 'Protein'), ('Protein', 'Water'), ('Water', 'Water')]:
        if args.verbose:
            print(f'{i} {j[0]}-{j[1]}...')
        subprocess.run(f'echo -e "{j[0]}\n{j[1]}" | gmx hbond-legacy -f materials/{i}.xtc -s materials/{i}.tpr -hbn results/{i}.{j[0]}-{j[1]}.legacy.ndx -num results/{i}.{j[0]}-{j[1]}.num.legacy.xvg -dist results/{i}.{j[0]}-{j[1]}.dist.legacy.xvg -ang results/{i}.{j[0]}-{j[1]}.ang.legacy.xvg -dan results/{i}.{j[0]}-{j[1]}.dan.legacy.xvg -nomerge', shell=True, capture_output=True, text=True)
